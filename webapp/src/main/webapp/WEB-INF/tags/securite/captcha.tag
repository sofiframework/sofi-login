<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles"  prefix="tiles"%>

<%@ attribute name="clePublique" required="true" type="java.lang.String"%>

<p>
  <script type="text/javascript">
    var RecaptchaOptions = {
      theme : 'white'
    };
  </script>
  <script type="text/javascript"
    src="http://www.google.com/recaptcha/api/challenge?k=${clePublique}">
  </script>
  <noscript>
    <iframe src="http://www.google.com/recaptcha/api/noscript?k=${clePublique}"
            height="300" width="500" frameborder="0"></iframe><br>
     <textarea name="recaptcha_challenge_field" rows="3" cols="40">
     </textarea>
     <input type="hidden" name="recaptcha_response_field" value="manual_challenge">
  </noscript>
</p>