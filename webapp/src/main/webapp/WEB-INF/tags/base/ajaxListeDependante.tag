<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="var" required="true" type="java.lang.String"%>
<%@ attribute name="href" required="true" type="java.lang.String"%>
<%@ attribute name="idListeDeroulanteParent" required="true" type="java.lang.String"%>
<%@ attribute name="idListeDeroulanteEnfant" required="true" type="java.lang.String"%>

<sofi-html:link ajax="true" 
  href="${href}" 
  idProprieteValeur="${idListeDeroulanteParent}"
  idRetourAjax="${idListeDeroulanteEnfant}" 
  var="${var}"
  property="${idListeDeroulanteParent}"
  nomParametreValeur="${idListeDeroulanteParent}"
  />
