<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="id" required="true" type="java.lang.String"%>
<%@ attribute name="property" required="true" type="java.lang.String"%>

<sofi-html:text 
  styleId="${id}" 
  property="${property}" 
  size="12" maxlength="10" 
  onfocus="afficherCalendrier(this, 'yy-mm-dd');" />