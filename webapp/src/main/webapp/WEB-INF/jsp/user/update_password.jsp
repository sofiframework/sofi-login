<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<div class="box">

  <sofi:barreStatut afficherSeulementSiMessage="true" exclureMessageErreurDefaut="true" />
  
	<sofi-html:form action="/changePassword.do?method=save" 
	                focusAutomatique="true" transactionnel="false"
	                styleClass="clearfix">
	    <h2 class="title-services"><sofi:libelle identifiant="login.label.updatePassword.title" /></h2>	    
	  <p>
	      <sofi:libelle identifiant="login.label.updatePassword.description" />
	    </p>
	   <ul class="formList">
	       <sofi-html:hidden 
	                     property="codeUtilisateur" 
	                     />
	      <li class="clearfix">    
	       <sofi:libelle identifiant="login.label.login.field.newPassword" 
	                      var="nouveauMotPasse" />
	       <sofi-html:password 
	            property="nouveauMotPasse" 
	            obligatoire="true"
	            size="30"
	            placeholder="${nouveauMotPasse}"
	            />
	     </li> 
	        <li class="clearfix">    
	        <sofi:libelle identifiant="login.label.login.field.confirmNewPassword" 
	                      var="confirmerMotPasse" />
	        <sofi-html:password
	             property="confirmerMotPasse" 
	             obligatoire="true"
	             size="30"
	             placeholder="${confirmerMotPasse}"
	             />
	        </li> 
	   </ul>       
	
	<div class="zoneBouton clearfix">
	  <sofi-html:submit libelle="login.label.updatePassword.action.send" />
    <sofi-html:link libelle="login.label.common.action.cancel" 
                      href="changePassword.do?method=cancel" /> 	  


	</div>
	   </sofi-html:form>
</div>  

