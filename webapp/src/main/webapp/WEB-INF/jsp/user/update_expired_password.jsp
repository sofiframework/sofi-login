<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<div class="box">
  <sofi-html:form action="/changeExpiredPassword.do?method=save" transactionnel="false" focusAutomatique="true">
	
	<sofi:barreStatut afficherSeulementSiMessage="true" exclureMessageErreurDefaut="true" />
		
	<p>  
	 <c:choose>
	  <c:when test="${pwd_expired}">
			<div id="message_erreur">
				<div id="messageFormulaire" class="zoneMessageFormulaireSimple">
				  <span class="message_tout_type message_icone_avertissement">
				   <sofi:message identifiant="login.warning.updateExpiredPassword.passwordExpired" />
				  </span>
				</div>
			  <div class="clear"></div>
			</div>
	  </c:when>
	  <c:when test="${new_pwd}">
	    <div id="message_erreur">
           <div id="messageFormulaire" class="zoneMessageFormulaireSimple">
             <span class="message_tout_type message_icone_avertissement">
               <sofi:message identifiant="login.info.updateExpiredPassword.explication" />
             </span>
           </div>
           <div class="clear"></div>
      </div>
	  </c:when>
		</c:choose>
	</p>
  <ul class="formList">
    <li class="clearfix">
      <sofi-html:hidden property="codeUtilisateur" />
    </li>
    <li class="clearfix">         
      <sofi:libelle identifiant="login.label.login.field.newPassword" 
                     var="nouveauMotPasse" 
                     />
      <sofi-html:password property="nouveauMotPasse" size="30" placeholder="${nouveauMotPasse}" />
    </li>
    <li class="clearfix">       
      <sofi:libelle identifiant="login.label.login.field.confirmNewPassword" 
                     var="confirmerMotPasse" />
      <sofi-html:password property="confirmerMotPasse" size="30" placeholder="${confirmerMotPasse}"/>
    </li>   
    <div class="zoneBouton clearfix">
      <sofi-html:submit libelle="login.label.updateExpiredPassword.action.send" />
      <sofi-html:link href="index.html" libelle="login.label.common.action.cancel" />
    </div>
  </sofi-html:form>
</div>  