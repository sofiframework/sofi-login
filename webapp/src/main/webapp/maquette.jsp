<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-tiles" prefix="sofi-tiles"%>

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="app-dialog" tagdir="/WEB-INF/tags/dialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true">
<head>
  <tiles:importAttribute name="titreApplication" />
  <tiles:importAttribute name="titre" ignore="true" />
  <tiles:importAttribute name="pageName" ignore="true" />
  <sofi:parametreSysteme code="loginOrganizationName" var="titrePage"/>
  <c:if test="${titre != null}">
    <sofi:libelle var="titreService" identifiant="${titre}" />
    <c:set var="titrePage" value="${titreService} - ${titrePage}" />
  </c:if>
  <title><c:out value="${titrePage}"/></title> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <sofi:css href="wro/login.css"></sofi:css>
  <!--[if IE 7]>
    <sofi:css href="css/ie7.css"></sofi:css>
  <![endif]-->
  <sofi:javascript src="wro/login.js"></sofi:javascript>
  <!-- afficher le calendrier dans la langue de l'utilisateur -->
  <c:if test="${(codeLangueEnCours != null) && (codeLangueEnCours == 'en_US')}">
    <sofi:javascript src="script/calendrier/lang/calendar-en.js"></sofi:javascript>
  </c:if>
  <c:if test="${(codeLangueEnCours == null) || (codeLangueEnCours == 'fr_CA')}">
    <sofi:javascript src="script/calendrier/lang/calendar-ca-fr.js"></sofi:javascript>
  </c:if>
</head>
<tiles:useAttribute name="menu" ignore="true" />
<tiles:useAttribute name="filnavigation" ignore="true" />
<tiles:useAttribute name="onglet" ignore="true" />
<tiles:useAttribute name="sommaire" ignore="true" />
<tiles:useAttribute name="document_electronique" ignore="true" />
<body>
	
<sofi:parametreSysteme code="urlImageCompany" var="urlImageCompany" />	

  <div id="background" class="${pageName}">
   <div id="contenant">
  	  <tiles:insert attribute="entete"/>
      <app:listeLangueSupportee libelle="login.libelle.commun.liste_langue" 
                                 styleClass="listeLangue"/>
      <div id="corps">
        <tiles:insert attribute="detail"></tiles:insert>
      </div>

      <div class="clear"></div>

      <div id="bas">
        <tiles:insert attribute="bas_page"/>
      </div>
    </div>
  </div>

  <app:fenetreAttente styleId="attenteAjax" libelle="En traitement, veuillez attentre un moment svp"></app:fenetreAttente>

</body>
<sofi:javascript appliquerOnLoad="true" />
</html:html>

<script>
	$(".closeSession").on("click",function(){
		var url = $(this).find("a").attr("href");
		window.location = url;
	});
	$(".closeSession a").on("click",function(e){
		e.preventDefault();
	});
</script>
