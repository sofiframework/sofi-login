<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ attribute name="libelle" required="true" type="java.lang.String"%>

<sofi-html:bouton onclick="closeDialog(this);" 
                  libelle="${libelle}"
                  styleId="btFermer" 
                  actifSiFormulaireLectureSeulement="true" 
                  activerConfirmationModificationFormulaire="true" />