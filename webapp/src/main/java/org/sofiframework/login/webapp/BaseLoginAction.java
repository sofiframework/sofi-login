package org.sofiframework.login.webapp;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.login.model.ModelLogin;
import org.sofiframework.presentation.struts.controleur.BaseDispatchAction;

public abstract class BaseLoginAction extends BaseDispatchAction {

  protected ModelLogin getModelLogin(HttpServletRequest request) {
    return (ModelLogin) getModele(request);
  }
}
