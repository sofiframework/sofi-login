<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<div id="entete_utilisateur">
   <ul>
    <c:forEach items="${listeUtilisateurAccueil}" var="utilisateurSession">
      <li>
        <a class="actif" href="mailto:<c:out value="${utilisateurSession.utilisateur.courriel}"></c:out>">
              <c:out value="${utilisateurSession.utilisateur.prenom}"></c:out> 
              <c:out value="${utilisateurSession.utilisateur.nom}"></c:out>
        </a>
      </li>
    </c:forEach>
  </ul>
</div>

