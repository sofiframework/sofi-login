<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ attribute name="divId" required="true" type="java.lang.String"%>
<%@ attribute name="titre" required="true" type="java.lang.String"%>

<div id="${divId}" title="${titre}" class="dialog">
  <jsp:doBody />
</div>
