<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles"  prefix="tiles"%>

<table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
  <jsp:doBody />
</table>