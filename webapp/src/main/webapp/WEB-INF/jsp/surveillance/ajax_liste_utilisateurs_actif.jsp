<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<fieldset> 
  <legend><sofi:libelle identifiant="infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.titre" /></legend>
    <sofi-liste:listeNavigation id="utilisateur"
                                   class="tableresultat"
                                   nomListe="${listeUtilisateur}"
                                   action="surveillance.do?methode=afficherListeUtilisateur" 
                                   colgroup="15%,30%,15%,15%,35%" 
                                   triDefaut="1"
                                   ajax="true"
                                   messageListeVide="infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.aucun_utilisateur">
      <sofi-liste:colonne property="utilisateur.codeUtilisateur" libelle="infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.code" trier="true"/>
      <sofi-liste:colonne property="nomComplet" libelle="infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.nom" trier="true"  paramId="codeUtilisateur" paramProperty="cleUtilisateur" href="surveillance.do?methode=afficherUtilisateur"/>
      <sofi-liste:colonne property="creationAcces" libelle="infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.cree_acces" trier="true" />
      <sofi-liste:colonne property="dernierAcces" libelle="infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.dernier_acces" trier="true" />
      <sofi-liste:colonne property="derniereAction" libelle="infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.derniere_action" trier="true" />
    </sofi-liste:listeNavigation>
</fieldset>