<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ taglib prefix="app-securite" tagdir="/WEB-INF/tags/securite" %>

<div class="box">
    <sofi-html:form action="/forgetPassword.do?method=send" 
                    transactionnel="false" focusAutomatique="true"
                    styleClass="clearfix">
      <sofi:barreStatut afficherSeulementSiMessage="true" exclureMessageErreurDefaut="true" />
        <h2 class="title-services">
          <sofi:libelle identifiant="login.label.forgetPassword.title" />
        </h2>
        <p>
          <sofi:libelle identifiant="login.label.forgetPassword.description" />
        </p>
        <ul class="formList">
          <li class="clearfix">
	          <sofi:libelle 
	            identifiant="login.label.login.field.userName" 
	            var="codeUtilisateur" />
	          <sofi-html:text 
	            property="codeUtilisateur" 
	            size="30"
	            placeholder="${codeUtilisateur}"
	            />
          </li>                          
        </ul>
        <%-- 
        <div id="captcha">
          <sofi:libelle 
            identifiant="login.label.forgetPassword.field.captcha" 
            nomAttribut="recaptcha_response_field" />
          <app-securite:captcha clePublique="6LdXEr0SAAAAAFBPYIb8wCur7qnVQIarjzbKhAdU" />
        </div>
        --%>
    
      <p class="zoneBouton">       
        <sofi-html:submit libelle="login.label.forgetPassword.action.send" />
        <sofi-html:link href="index.html" libelle="login.label.common.action.cancel" />
      </p>
    </sofi-html:form>
</div>
