package org.sofiframework.login.model.local;

import org.sofiframework.application.securite.service.ServiceApplication;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.application.spring.securite.service.ServiceUtilisateur;
import org.sofiframework.application.courriel.service.ServiceCourriel;

import org.sofiframework.login.model.ModelLogin;

public class ModelLoginImpl extends
    org.sofiframework.modele.spring.ModeleImpl implements ModelLogin {
  
  
  private ServiceApplication serviceApplication;
  private ServiceUtilisateur serviceUtilisateur;
  private ServiceAuthentification serviceAuthentification;
  private ServiceCourriel serviceCourriel;
  
  public ServiceApplication getServiceApplication() {
    return serviceApplication;
  }
  public void setServiceApplication(ServiceApplication serviceApplication) {
    this.serviceApplication = serviceApplication;
  }
  public ServiceUtilisateur getServiceUtilisateur() {
    return serviceUtilisateur;
  }
  public void setServiceUtilisateur(ServiceUtilisateur serviceUtilisateur) {
    this.serviceUtilisateur = serviceUtilisateur;
  }
  public ServiceAuthentification getServiceAuthentification() {
    return serviceAuthentification;
  }
  public void setServiceAuthentification(ServiceAuthentification serviceAuthentification) {
    this.serviceAuthentification = serviceAuthentification;
  }
  public ServiceCourriel getServiceCourriel() {
    return serviceCourriel;
  }
  public void setServiceCourriel(ServiceCourriel serviceCourriel) {
    this.serviceCourriel = serviceCourriel;
  }
  
  

}
