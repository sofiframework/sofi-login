package org.sofiframework.login.webapp.user.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetTransfert;

import org.sofiframework.login.webapp.FormAction;

/**
 * Contrôle les actions effectuées dans la page 
 * de profil de l'utilisateur.
 * 
 * @author Jean-Maxime Pelletier
 */
public class UtilisateurAction extends FormAction {
  
  /**
   * Consulter le profil.
   */
  public ActionForward view(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
      
    this.access(mapping, form, request, response);
    
    return mapping.findForward("ajax");
  }

  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    Utilisateur utilisateur = null;
    try {
      String certificat = request.getParameter("certificat");
      String codeUtilisateur = GestionSecurite.getInstance().getServiceAuthentification().getCodeUtilisateur(certificat);
      utilisateur = GestionSecurite.getInstance().getUtilisateur(codeUtilisateur);
    } catch (Exception e) {
      throw new SOFIException("Erreur pour obtenir l'utilisateur.", e);
    }
    return utilisateur;
  }
}
