package org.sofiframework.login.model;

import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.transaction.support.DefaultTransactionStatus;

import org.sofiframework.modele.exception.ModeleException;

public class TransactionManager extends HibernateTransactionManager {

  private static final long serialVersionUID = 6082974998222635569L;

  @Override
  protected void doCommit(DefaultTransactionStatus statut) {
    try {
      super.doCommit(statut);  
    } catch (Exception e) {
      throw new ModeleException("Erreur lors du commit de la transaction.", e); 
    }
  }
}
