package org.sofiframework.login.webapp.home.form;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * Formulaire de l'accueil.
 * 
 * @author Jean-Maxime Pelletier
 */
public class HomeForm extends BaseForm {

  private static final long serialVersionUID = 1202059261317793228L;

  /**
   * Le code d'application à afficher dans le tableau de bord.
   */
  private String codeApplication;

  /**
   * Valide le formulaire.
   * @param request la requete HTTP en traitement
   */
  public void validerFormulaire(HttpServletRequest request) {
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }
}
