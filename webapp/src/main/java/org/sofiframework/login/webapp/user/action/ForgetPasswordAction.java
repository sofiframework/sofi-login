package org.sofiframework.login.webapp.user.action;

import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.courriel.service.ServiceCourriel;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.objetstransfert.ObjetTransfert;

import org.sofiframework.login.webapp.FormAction;
import org.sofiframework.login.webapp.user.form.ForgetPasswordForm;

/**
 * Action qui envoi une un courriel à l'utilisateur qui a oublié son mot de
 * passe. Le courriel contient l'url vers la page de changement du mot de passe.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ForgetPasswordAction extends FormAction {

  private static final Log log = LogFactory.getLog(ForgetPasswordAction.class);

  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    // Cette page n' pas d'objet de transfert
    return new org.sofiframework.objetstransfert.ObjetCleValeur();
  }

  /**
   * Envoi le courriel à l'utilisateur.
   */
  public ActionForward send(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    ForgetPasswordForm formulaire = (ForgetPasswordForm) form;

    // Extraire la langue de l'utilisateur.
    Locale locale = getLocale(request);
    
    try {

      // Extraire le code utilisateur
      String codeUtilisateur = formulaire.getCodeUtilisateur();

      // Extaire l'utilisateur selon son code.
      Utilisateur utilisateur = this.getModelLogin(request).getServiceUtilisateur().getUtilisateur(codeUtilisateur);
      
      ServiceCourriel serviceCourriel = getModelLogin(request).getServiceCourriel();

      if (utilisateur != null) {

        // Générer un certificat afin de permettre l'envoi d'un URL pour le
        // changement de mot de passe.
        String certificat = GestionSecurite.getInstance()
            .getServiceAuthentification().genererCertificat(
                utilisateur.getCodeUtilisateur());
        
        
        Application application = GestionSecurite.getInstance().getApplication();

        // Extraire la base de l'url.
        String urlEnCours = application.getAdresseSite();

        // Génération de l'url pour permettre le changement de mot de passe.
        String urlChangerMotPasse = urlEnCours.toString()
            + "/changePassword.do?method=access&certificat=" + certificat;

        // Extraire le contenu du message.
        Message contenuMessage = UtilitaireLibelle.getLibelle(
            "login.email.forgetPassword.body", request,
            new Object[] { urlChangerMotPasse });
        
        
        HashMap<String, Object> detail = new HashMap<String, Object>();
        
        String courrielBaseUrl = GestionParametreSysteme.getInstance().getParametreSysteme("courrielBaseUrl").toString();
        detail.put("baseEmailUrl", courrielBaseUrl);
        detail.put("template","login.label.email.template");
        detail.put("footer","login.email.common.footer");
        
        String courrielFrom = "system@sunmedia.ca";
        String descriptionFrom =  "system@sunmedia.ca";
        
        HashMap<String,String> headers = new HashMap<String,String>();
        
        detail.put("headers", headers);
        
        serviceCourriel.
        		envoyer("login.email.forgetPassword.subject", null, contenuMessage.getMessage(), 
        				detail, utilisateur.getCourriel(), utilisateur.getPrenom(), utilisateur.getNom(),
        				courrielFrom, descriptionFrom, locale.toString(), null);

        formulaire.ajouterMessageInformatif(
            "login.information.oublier_mot_passe.confirmer_envoi_courriel",
            request);
      }else {
        formulaire.ajouterMessageErreur("login.erreur.oublier_mot_passe_codeUtilisateur.invalide", "codeUtilisateur", request);  
      }

      formulaire.ajouterMessageInformatif(
          "login.information.oublier_mot_passe.confirmer_envoi_courriel",
          request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          "login.information.oublier_mot_passe.echec_envoi_courriel",
          request);
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de l'envoi de courriel.", e);
      }
    }

    return mapping.findForward("show");
  }
}