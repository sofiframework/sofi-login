SET DEFINE OFF;

REM INSERTING into INFRA_SOFI.APPLICATION
-----------------------------------------
Insert into INFRA_SOFI.APPLICATION (CODE_APPLICATION,NOM,DESCRIPTION,ACRONYME,IND_COMMUN,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,CODE_STATUT,CODE_UTILISATEUR_ADMIN,MOT_PASSE_ADMIN,ADRESSE_SITE,VERSION_LIVRAISON,CLE_PAGE_MAINTENANCE,CLE_PAGE_ERREUR,CLE_PAGE_SECURITE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION,VERSION,IND_EXTERNE,ADRESSE_CACHE) values ('LOGIN','Sun Media | Login','L''application d''authentification de SOFI',null,'N',null,null,'A','infra_sofi','infra_sofi','http://paywall-prod-admin-01.dmz90.in.sunmedia.org/login','3.0',null,null,null,'sofi',to_date('18/11/11','DD/MM/RR'),'sofi',to_date('15/08/12','DD/MM/RR'),'3',null,null);

REM INSERTING into INFRA_SOFI.ROLE
---------------------------------
Insert into INFRA_SOFI.ROLE (CODE_ROLE,CODE_APPLICATION,NOM,DESCRIPTION,IND_CONSULTATION,CODE_STATUT,CODE_ROLE_PARENT,CODE_APPLICATION_PARENT,CODE_TYPE_UTILISATEUR,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION,CODE_FACETTE,VERSION) values ('PILOTE','LOGIN','Pilote',null,null,'A',null,null,null,'system',to_date('18/11/11','DD/MM/RR'),null,null,null,'0');
