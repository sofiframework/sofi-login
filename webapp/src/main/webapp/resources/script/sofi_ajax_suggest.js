
function getNomNested(nom, composant) {
  var idComposant = $(composant).attr('id');
  var indexNested = idComposant.lastIndexOf('.');
  var nomNested = idComposant.substring(0, indexNested);
  
  nomNested = nomNested.replace('[', '\\[');
  nomNested = nomNested.replace(']', '\\]');
  nomNested = nomNested.replace('.', '\\.');
  
  return nomNested + '\\.' + nom;
}

function initialiserAutoComplete(nomChampIdentifiant, nomChampDescription, url, composant) {
  var idComposant = $(composant).attr('id');
  var indexNested = idComposant.lastIndexOf('.');
  
  if (indexNested != -1) {
    nomChampIdentifiant = getNomNested(nomChampIdentifiant, composant);
    nomChampDescription = getNomNested(nomChampDescription, composant);
  }

  var champIdentifiant = $('#' + nomChampIdentifiant);
  var champDescription = $('#' + nomChampDescription);

  if (champDescription.attr('autocomplete') == null) {
    var options = {
        multiple: false,
        mustMatch: true,
        highlightItem: true,
        formatItem: function(item) {
          var retour = item[1];
          return retour;
        },
        formatResult: function(row) {
          var retour = row[1];
          return retour;
        }
      };
      champDescription.autocomplete(url, options).result(
          function(event, item) {
            if (item != null) {
              item = item[0]
            } else {
              item = '';
            }
            champIdentifiant.attr('value', item);
            champIdentifiant.change();
            champDescription.change();
          }
       );
  }
}
