package org.sofiframework.login.webapp.controller.rest.v1;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/api/v1")
@Controller
public class SofiSsoController {

	@Autowired
	private ServiceAuthentification serviceAuthentification;

	@Autowired
	@Qualifier("serviceSecurite")
	private ServiceSecurite serviceSecurite;

	@RequestMapping("/ping")
	@ResponseBody
	public String ping() {
		return "{ status : 'ok' }";
	}

	@RequestMapping("/application/{codeApplication}/utilisateur/{codeUtilisateur:.+}/certificat")
	@ResponseBody
	public String genererCertificat(@PathVariable String codeUtilisateur,
			HttpServletRequest request) {
		Integer nbJours = getIntParam("nbJourExpiration", 1, request);
		String ip = request.getParameter("ip");
		String userAgent = request.getParameter("userAgent");
		String urlReferer = request.getParameter("urlReferer");
		String certificat = this.serviceAuthentification.genererCertificat(
				codeUtilisateur, nbJours, ip, userAgent, urlReferer);
		return certificat;
	}

	@RequestMapping(value = "/certificat/{codeUtilisateur}", method = RequestMethod.DELETE)
	@ResponseBody
	public String terminerCertificat(@PathVariable String codeUtilisateur) {
		this.serviceAuthentification
				.terminerCertificatPourCode(codeUtilisateur);
		return "{ status : 'ok' }";
	}

	/**
	 * Extraire les informations d'un utilisateur
	 * 
	 * @param codeUtilisateur
	 * @param codeUtilisateur
	 * @return Les informations d'un utilisateur en JSON format.
	 * @throws JSONException 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/application/{codeApplication}/utilisateur/{codeUtilisateur:.+}")
	public ResponseEntity<String> getUtilisateur(
			@PathVariable("codeApplication") String codeApplication,
			@PathVariable("codeUtilisateur") String codeUtilisateur,
			HttpServletRequest request) throws JSONException {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		headers.add("Access-Control-Allow-Origin", "*");

		Utilisateur utilisateur = this.serviceSecurite
				.getUtilisateurAvecObjetsSecurises(codeApplication,
						codeUtilisateur);
		if (utilisateur != null && utilisateur.getAccesApplication() != null) {
			// Prepare response in JSON.
			JSONObject response = new JSONObject();
			response.accumulate("id", utilisateur.getId());
			response.accumulate("codeUtilisateur", utilisateur.getCodeUtilisateur());
			response.accumulate("prenom", utilisateur.getPrenom());
			response.accumulate("nom", utilisateur.getNom());
			response.accumulate("courriel", utilisateur.getCourriel());
			response.accumulate("sexe", utilisateur.getSexe());
			response.accumulate("locale", utilisateur.getLocale());
			
			// List of authorize actions depending on role
			JSONArray listeComposants = new JSONArray();
			
			for(Object object : utilisateur.getListeObjetSecurisables()){
			  ObjetSecurisable objetSecurisable = (ObjetSecurisable) object;
			  JSONObject composant = new JSONObject();
			  composant.accumulate("nomAction", objetSecurisable.getNomAction());
			  listeComposants.put(composant);
			}
			response.put("listeComposants", listeComposants);

			// List of roles
			JSONArray listRoles = new JSONArray();
			
			for (Role r : utilisateur.getListeRoles()) {
				if (r.getCodeApplication().equals(codeApplication)) {
					// Prepare response in JSON.
					JSONObject role = new JSONObject();
					role.accumulate("code", r.getCode());
					role.accumulate("nom", r.getNom());
					role.accumulate("description", r.getDescription());
					role.accumulate("consultationSeulement", r.isConsultationSeulement());
					listRoles.put(role);
				}
			}

			response.put("listeRoles", listRoles);
			
			// Prepare response in JSON.
			JSONObject accesApplication = new JSONObject();
			accesApplication.accumulate("codeStatut", utilisateur.getAccesApplication().getCodeStatut());
			accesApplication.accumulate("codeApplication", utilisateur.getAccesApplication().getCodeApplication());
			
			response.put("accesApplication", accesApplication);

			return new ResponseEntity<String>(response.toString(), headers,
					HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(headers, HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping("/certificat/{certificat:.+}")
	@ResponseBody
	public String getCodeUtilisateur(@PathVariable String certificat) {
		String resultat = this.serviceAuthentification
				.getCodeUtilisateur(certificat);
		return resultat;
	}

	private Integer getIntParam(String nom, int defaut,
			HttpServletRequest request) {
		Integer nombre = defaut;
		String valeur = request.getParameter(nom);
		if (valeur != null) {
			try {
				nombre = Integer.parseInt(valeur);
			} catch (Exception e) {
				// on laisse la valeur par défaut
			}
		}
		return nombre;
	}
}
