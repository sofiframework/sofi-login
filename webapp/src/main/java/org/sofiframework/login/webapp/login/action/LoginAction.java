package org.sofiframework.login.webapp.login.action;

import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.utilitaire.UtilitaireRequest;

import org.sofiframework.login.webapp.BaseLoginAction;

public class LoginAction extends BaseLoginAction {
  @Override
  protected ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    if ("fail".equals(request.getParameter("error"))) {
      request.getSession().setAttribute("FAIL", Boolean.TRUE);
    }

    deleteAuthCookies(request, response);

    return mapping.findForward("show");
  }

  public ActionForward logout(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {

    request.getSession().setAttribute("SOFI_LOGOUT", Boolean.TRUE);

    deleteAuthCookies(request, response);

    return mapping.findForward("show");
  }

  public ActionForward show(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {

    String urlRetour = (String) request.getSession().getAttribute("url_retour");

    Boolean logout = (Boolean) request.getSession().getAttribute("SOFI_LOGOUT");

    Boolean fail = (Boolean) request.getSession().getAttribute("FAIL");

    String locale = request.getParameter("locale");

    if (locale == null) {
      locale = getLocale(request).toString();
    }

    if (fail == null && logout != null) {
    	request.getSession().invalidate();
    }

    if (logout != null) {
      request.setAttribute("SOFI_LOGOUT", Boolean.TRUE);
    }

    if (urlRetour != null) {
      request.getSession().setAttribute("url_retour", urlRetour);
    }

    if (fail != null) {
      request.getSession().setAttribute("LOGIN_ERROR_MESSAGE",
          "login.erreur.authentification.codeUtilisateur.invalide");
    }

    if (locale != null) {
      Locale localeEnCours = UtilitaireControleur.getLocale(locale);
      setLocale(request, localeEnCours);
    }

    return mapping.findForward("page");
  }

  private void deleteAuthCookies(HttpServletRequest request, HttpServletResponse response) {
    // Supprime le cookie
    Cookie authCookieASupprimer = new Cookie(GestionSecurite.getInstance().getNomCookieCertificat(), "");
    authCookieASupprimer.setMaxAge(0);
    authCookieASupprimer.setPath("/");

    String domain = GestionSecurite.getInstance().getNomDomaineCookie();
    authCookieASupprimer.setDomain(domain);
    UtilitaireRequest.supprimerCookie(response, authCookieASupprimer, "/");

    Cookie authCookieASupprimer2 = new Cookie(GestionSecurite.getInstance().getNomCookieCertificat(), "");
    authCookieASupprimer2.setMaxAge(0);
    authCookieASupprimer2.setPath("/");

    UtilitaireRequest.supprimerCookie(response, authCookieASupprimer2, "/");

  }

}
