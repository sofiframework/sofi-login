<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sofi" uri="http://www.sofiframework.org/taglib/sofi"%>
<%@ taglib prefix="sofi-html" uri="http://www.sofiframework.org/taglib/sofi-html"%>
<style>
	#corps {
	  width:50%;
	}
</style>

<div class="box">
  <form class="clearfix" action="j_spring_security_check" method="POST">
    <h2> <sofi:libelle identifiant="login.label.login.title.signIn" /> </h2>
    
    <sofi:libelle identifiant="login.label.login.field.userName" var="lusername" />
    <sofi:libelle identifiant="login.label.login.field.password" var="lpassword" />
    
    <div class="clearfix">

    <ul class="formList">
      <li class="clearfix">
         <sofi:libelle identifiant="login.label.login.field.userName"  nomAttribut="j_username" styleClass="username"/>
         <sofi-html:text  styleId="j_username"
                          property="j_username" 
                          size="30" 
                          maxlength="100"
                          autocomplete="false"
                          placeholder="${lusername}"
                          />

      </li>

      <li class="clearfix">
        <sofi:libelle identifiant="login.label.login.field.password" nomAttribut="j_password" styleClass="password" />
        <sofi-html:password  styleId="j_password"
		                         property="j_password" 
		                         size="30"
		                         maxlength="30"
		                         autocomplete="false"
		                         placeholder="${lpassword}"
                            />
      </li>

      <li>
         <c:if test="${not empty LOGIN_ERROR_MESSAGE}">
        <div id="message_erreur">
            <div id="messageFormulaire" class="zoneMessageFormulaireSimple">
               <span class="message_tout_type message_icone_attention">
                <sofi:message identifiant="${LOGIN_ERROR_MESSAGE}" />
              </span>
            </div>
         <div class="clear"></div>
       </div>
      </c:if> 
      <c:if test="${not empty SOFI_LOGOUT}">
          <div id="message_erreur">
              <div id="messageFormulaire" class="zoneMessageFormulaireSimple">
                 <span class="message_tout_type message_icone_information">
                  <sofi:message identifiant="login.information.authentification.fermeture_session" />
                </span>
              </div>
              <div class="clear"></div>
         </div>
         <c:remove var="SOFI_LOGOUT"/>
      </c:if> 
      </li>
      <li class="clearfix stayConnected">
        <sofi:libelle identifiant="login.label.login.field.keep_me_signin" nomAttribut="_spring_security_remember_me" /> 
        <!--   <input type="checkbox" name="_spring_security_remember_me" value="on" class="radio" checked> -->
        
        <div id="rememberme" class="chkbox checked"></div>
        
        <input type='hidden' name="_spring_security_remember_me" id="_spring_security_remember_me" value="on">
      </li>
      
      <li class="forgetPassword">
        <sofi-html:link href="forgetPassword.do?method=access" libelle="login.label.login.action.forget_password" />
      </li>
  
      </ul>
      
      <div class="wrapper">
	      
	      <div class="zoneBouton clearfix">
		      <sofi:libelle identifiant="login.label.login.action.signIn"  var="libelleSubmit"/>
		      <button type="submit" value="${libelleSubmit}" class="bouton"><span>${libelleSubmit}</span></button>
	      </div>
      
      </div>
    </div>
    <script type="text/javascript">
      document.getElementById("j_username").focus();
    </script>
  </form>
</div>

<script>

$(".stayConnected label").on("click",function(){
	var elem = "#rememberme";
	ManageStayConnected(elem);
});
$("#rememberme").on("click",function(){
	  ManageStayConnected("#rememberme");
	});
	
function ManageStayConnected(elem){
  if($(elem).hasClass("checked")){
	  $(elem).removeClass("checked");
	  $(elem).addClass("unchecked");
    $("input[name=_spring_security_remember_me]").val("off");
  }
  else{
    $(elem).removeClass("unchecked");
    $(elem).addClass("checked");
    $("input[name=_spring_security_remember_me]").val("on");
  }
}

</script>



