<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<div id="innerFooterBox" class="clearfix">
    <sofi-html:link href="http://www.sofiframework.org"
                    styleClass="logo"
                    libelle="infra_sofi.libelle.commun.propulseParSofi"
                    target="_new"/>
    <p style="right:10px;text-align:right;">
        <sofi:libelle identifiant="infra_sofi.libelle.commun.version" /> <sofi:parametreSysteme code="releaseVersion" /> (${appStartDate})
    </p>
    <p><sofi:parametreSysteme code="releaseYear" var="releaseYear" />
      <sofi:libelle identifiant="infra_sofi.libelle.commun.copyright" parametre1="${releaseYear}" />
    </p>
</div>
<!-- /#pied-->