<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="urlListe" required="true" type="java.lang.String"%>
<%@ attribute name="divListe" required="true" type="java.lang.String"%>
<%@ attribute name="urlSuppression" required="true" type="java.lang.String"%>
<%@ attribute name="libelle" required="true" type="java.lang.String"%>
<%@ attribute name="disabled" required="true" type="java.lang.Boolean"%>

<sofi:message var="messageConfirmation" identifiant="infra_sofi.message.commun.confirmation_supprimer" />

<c:set var="jsSupprimer" value="
  if (confirm('${messageConfirmation}')) {
    var fonction = function() {
      ajaxUpdate('${urlListe}', '${divListe}');
    };
    var formulaire = $(this).parents('form');
    formulaire.attr('action', '${urlSuppression}&ignorerValidation=true');
    postDialog(this, fonction);
    inactiverBoutonsFormulaire(formulaire);
    fermerDialog(this);
  }"
 />
<sofi-html:bouton onclick="${jsSupprimer}" libelle="${libelle}" disabled="${disabled}" />