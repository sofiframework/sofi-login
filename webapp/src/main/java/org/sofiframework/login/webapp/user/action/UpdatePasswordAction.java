package org.sofiframework.login.webapp.user.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.securite.objetstransfert.ChangementMotPasse;

import org.sofiframework.login.model.ModelLogin;
import org.sofiframework.login.webapp.FormAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Contrôle les actions effectuées pour qu'un utilisateur 
 * modifie son mot de passe.
 * 
 * @author Jean-Maxime Pelletier
 */
public class UpdatePasswordAction extends FormAction {
  
  /**
   * Le code utilisateur doit être celui de l'utilisateur en cours.
   */
  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    ChangementMotPasse changement = new ChangementMotPasse();
    changement.setCodeUtilisateur(getUtilisateur(request).getCodeUtilisateur());
    return changement;
  }
  
  /**
   * Effectue le changement du mot de passe.
   */
  public ActionForward save(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    ChangementMotPasse changement = (ChangementMotPasse) getFormulaire(form).getObjetTransfert();
    
    ModelLogin modele = (ModelLogin) getModele(request);
    try {
      modele.getServiceUtilisateur().changerMotPasse(changement);
      getFormulaire(form).ajouterMessageInformatif("login.information.changePassord.update_confirm", request);
      return mapping.findForward("home");
    } catch (ModeleException e) {
      getFormulaire(form).ajouterMessageErreur(e, request);
    }
    return afficher(mapping);
  }  
  
  public ActionForward cancel(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    return mapping.findForward("home");
  }
}
