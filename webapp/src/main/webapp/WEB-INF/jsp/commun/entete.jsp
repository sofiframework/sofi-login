<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>

<sofi:parametreSysteme code="urlImageCompany" var="urlImageCompany" />  
  
<div id="logo_general">
  <div id="bouton_accueil"><a href="index.html" ><img src="${urlImageCompany}" /></a></div>
  <c:if test="${utilisateur != null}">
	  <div class="closeSession">
		  <sofi-html:link styleId="fermerSession" styleClass="blanc"
									    href="j_spring_security_logout"
									    libelle="infra_sofi.libelle.commun.fermer_session"
									    messageConfirmation="infra_sofi.confirmation.commun.fermer_session" />
		</div>						    
	  <div id="zone_utilisateur">
	    <p class="utilisateur">
	      <span> ${utilisateur.prenom}&nbsp;${utilisateur.nom}
	    </span>
	      (${utilisateur.codeUtilisateur})
	    </p>
	    <ul>
	      <li class="btn_aide"><sofi:aide styleId="aideGeneral" ajax="true"
	        styleClass="aideEnLigne" largeurfenetre="655" hauteurfenetre="400">
	        <sofi:libelle identifiant="infra_sofi.libelle.commun.aide_en_ligne" />
	      </sofi:aide></li>
	    </ul>
	  </div>
  </c:if>
</div>


<div class="clear"></div>