﻿<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-tiles" prefix="sofi-tiles"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>

<!-- fil_navigation -->
<p id="localisation">
   &nbsp;&nbsp;&nbsp;
  <sofi-menu:menu nom="Velocity" gabaritVelocity="/org/sofiframework/presentation/velocity/gabarit/fil_navigation.vm">
    <c:forEach var="composantMenu" items="${utilisateur.filNavigation}">
      <sofi-menu:section identifiant="${composantMenu.identifiant}" />
    </c:forEach>
  </sofi-menu:menu>
  <sofi-html:link
    styleClass="filnavigation"
    accesskey="${accessKey['tacheprecedente']}"
    appliquerAdresseRetour="true" 
    libelle="Retourner à la page précédente" 
    activerConfirmationModificationFormulaire="true" />
</p>
