package org.sofiframework.login.webapp.user.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.securite.objetstransfert.ChangementMotPasse;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;

import org.sofiframework.login.model.ModelLogin;
import org.sofiframework.login.webapp.FormAction;

import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Contrôle les actions effectuées pour qu'un utilisateur 
 * modifie son mot de passe.
 * 
 * @author Jean-Maxime Pelletier
 */
public class UpdateExpiredPasswordAction extends FormAction {
  
  /**
   * Le code utilisateur doit être celui de l'utilisateur en cours.
   */
  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    ChangementMotPasse changement = new ChangementMotPasse();
    changement.setCodeUtilisateur(getCodeUtilisateurEffectif(request));
    return changement;
  }
  
  private String getCodeUtilisateurEffectif(HttpServletRequest request) {
    Utilisateur utilisateur = getUtilisateur(request);
    return (utilisateur.getCodeUtilisateurApplicatif() != null) ? 
        utilisateur.getCodeUtilisateurApplicatif() : 
          utilisateur.getCodeUtilisateur();
  }

  /**
   * Effectue le changement du mot de passe.
   */
  public ActionForward save(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {

    ChangementMotPasse changement = (ChangementMotPasse) getFormulaire(form).getObjetTransfert();
    ModelLogin modele = (ModelLogin) getModele(request);
    try {
      modele.getServiceUtilisateur().changerMotPasseExpirer(changement);
      // Get the user
      Utilisateur utilisateur = getUtilisateur(request);
      utilisateur = modele.getServiceUtilisateur().getUtilisateurPourId(utilisateur.getId());
      if (!StringUtils.equals(utilisateur.getCodeStatut(), "A")) {
        utilisateur.setCodeStatut("A");
        modele.getServiceUtilisateur().enregistrerUtilisateur(utilisateur);
      }
      getFormulaire(form).ajouterMessageInformatif("login.erreur.authentification.motPasse.modifie_avec_succes", request);
    } catch (ModeleException e) {
      getFormulaire(form).ajouterMessageErreur(e, request);
      return afficher(mapping);
    }
    return mapping.findForward("chargerApplication");
  }  
}
