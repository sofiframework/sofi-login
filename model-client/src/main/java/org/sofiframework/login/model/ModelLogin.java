package org.sofiframework.login.model;

import org.sofiframework.application.courriel.service.ServiceCourriel;
import org.sofiframework.application.securite.service.ServiceApplication;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.application.spring.securite.service.ServiceUtilisateur;

public interface ModelLogin {
  
  public ServiceApplication getServiceApplication();
  public ServiceUtilisateur getServiceUtilisateur();
  public ServiceAuthentification getServiceAuthentification();
  public ServiceCourriel getServiceCourriel();
  

}
