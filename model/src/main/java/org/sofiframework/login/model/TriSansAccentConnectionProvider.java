package org.sofiframework.login.model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.connection.DatasourceConnectionProvider;


/**
 * Ce connection provider set les propriétées NLS configurer
 * les recherches avec les accents.
 * 
 * @author mhamel
 *
 */
public class TriSansAccentConnectionProvider extends DatasourceConnectionProvider {
  public Connection getConnection() throws SQLException {
    Connection connection = super.getConnection();
    Statement statement = connection.createStatement();
    statement.execute("ALTER SESSION SET NLS_COMP=LINGUISTIC");
    statement.execute("ALTER SESSION SET NLS_SORT=BINARY_AI");
    return connection;
 }
}
