<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>

<!-- titre -->
<div id="titre-aide-navigation">
  <div id="titre-page">
    <h1><sofi:libelle identifiant="${titreEntete}" /></h1>
  </div>
  <div id="outils">
    <sofi:aide styleId="aideGeneral"
              ajax="true"
              styleClass="aideEnLigne"  
              largeurfenetre="655" 
              hauteurfenetre="400">
      <div id="aide" class="aide" >
        <sofi:libelle identifiant="titre" styleClass="texte-aide" />
      </div>
    </sofi:aide>
  </div>
</div>
<div class="fixclear">&nbsp;</div>