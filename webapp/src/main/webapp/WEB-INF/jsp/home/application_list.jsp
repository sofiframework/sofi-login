<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<style>

#corps {
  padding-bottom: 10px!important;
  padding-top:5px!important;
}

</style>

<sofi:barreStatut afficherSeulementSiMessage="true" exclureMessageErreurDefaut="true" />
<div class="box">
  <h2 class="title-services">
   <sofi:libelle identifiant = "login.label.home.description" />
  </h2>
	<ul class="listeServices">
	  <sofi-liste:utilitaire var="nombreApplication" liste="${listeApplication}"/>
	  <c:forEach items="${listeApplication}" var="application" varStatus="compteurListe">
	    <c:if test="${compteurListe.index < (nombreApplication-1)}">
	       <li>
	    </c:if>
	    <c:if test="${compteurListe.index == (nombreApplication -1)}">
	     <li class="last">
	    </c:if>
	    <li>
	      <sofi-html:link href="${application.adresseSite}" libelle="${application.nom}" />
	    </li>
	  </c:forEach>
	</ul>
	<div class="password">
    <sofi-html:link href="changePassword.do?method=access" 
                 libelle="login.label.home.action.updatePassword" 
                 memoriserHrefActuel="true"
                 styleClass="changePassword" />  
	</div>  
	<c:if test="${lastLogin != null}">
		<div class="lastSignIn">
		   <fmt:formatDate value="${lastLogin.dateConnexion}" type="both" dateStyle="long" timeStyle="long" var="dernierAccces" />
		   <sofi:libelle identifiant="login.label.home.lastSignin" parametre1="${dernierAccces}" />
	</div>  
	</c:if>          
</div>

