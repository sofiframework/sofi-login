package org.sofiframework.login.webapp.home.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.filtre.FiltreCertificat;
import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.objetstransfert.CertificatAcces;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.utilitaire.UtilitaireObjet;

import org.sofiframework.login.model.ModelLogin;
import org.sofiframework.login.webapp.BaseLoginAction;
import org.sofiframework.login.webapp.home.form.HomeForm;

/**
 * Action d'accueil.
 */
public class HomeAction extends BaseLoginAction {

  private static final String URL_RETOUR = "url_retour";

  @Override
  protected ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    return mapping.findForward("show");
  }

  /**
   * Porte d'entrer à l'unité de traitement.
   */
  @SuppressWarnings("rawtypes")
  public ActionForward access(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {

    ActionForward direction = null;
    String urlRetour = (String) request.getParameter(URL_RETOUR);

    if (StringUtils.isBlank(urlRetour)) {
      urlRetour = (String) request.getSession().getAttribute(URL_RETOUR);
    }

    String nomCertificat = GestionSecurite.getInstance().getNomCookieCertificat();
    String certificat = (String) request.getSession().getAttribute(nomCertificat);

    if (urlRetour != null) {

      // Supprimer cette variable de la session
      request.getSession().removeAttribute(URL_RETOUR);

      try {
        urlRetour = urlRetour + (urlRetour.indexOf("?") != -1 ? "&" : "?") + nomCertificat + "=" + certificat;
        response.sendRedirect(urlRetour);
      } catch (IOException e) {
        throw new ServletException("Erreur de redirection.", e);
      }
      direction = null;
    } else {
      HomeForm formulaire = (HomeForm) form;
      // Fixer l'application sélectionné et la conservé dans la session pour
      // sélection
      // par défaut des autres services.
      if (formulaire.getCodeApplication() != null) {
        request.getSession().setAttribute("systeme_selectionne", formulaire.getCodeApplication());
      }

      Utilisateur user = getUtilisateur(request);
      List<Application> liste = user.getListeApplications();

      List<Application> servicesList = new ArrayList<Application>();

      if (liste != null) {
        for (Iterator<Application> iterator = liste.iterator(); iterator.hasNext();) {
          Application application = iterator.next();
          String url = application.getAdresseSite() + (application.getAdresseSite().indexOf("?") != -1 ? "&" : "?")
              + GestionSecurite.getInstance().getNomCookieCertificat() + "=" + certificat;
          Application cloneApp = new Application();
          UtilitaireObjet.copierAttribut(cloneApp, application);
          cloneApp.setAdresseSite(url);
          servicesList.add(cloneApp);
        }

        Iterator<Application> iterator = servicesList.iterator();
        while (iterator.hasNext()) {
          Application application = iterator.next();
          if (application.getCommun()) {
            iterator.remove();
          }
        }

        request.getSession().setAttribute("listeApplication", servicesList);

      } else {
        request.getSession().setAttribute("listeApplication", new ArrayList());
      }

      direction = mapping.findForward("show");
    }

    return direction;
  }

  public ActionForward show(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {

    ModelLogin model = (ModelLogin) getModelLogin(request);

    Utilisateur user = getUtilisateur(request);

    if (user != null) {
      setLastLogin(model, user, request);
    }

    if (request.getSession().getAttribute("listeApplication") == null) {
      access(mapping, form, request, response);
    }

    return mapping.findForward("page");
  }

  public ActionForward quit(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    request.getSession().invalidate();

    return mapping.findForward("show");
  }

  private void setLastLogin(ModelLogin model, Utilisateur user, HttpServletRequest request) {

    if (request.getSession().getAttribute("lastLogin") == null) {
      FiltreCertificat filter = new FiltreCertificat();
      filter.setCodeUtilisateur(new String[] { user.getCodeUtilisateur() });
      // Init the navigation list
      ListeNavigation accessList = appliquerListeNavigation("accessList", request);
      accessList.setMaxParPage(new Integer(2));
      accessList.setFiltre(filter);
      accessList.ajouterTriInitial("dateConnexion", false);
      accessList = model.getServiceAuthentification().getListeCertificat(accessList);
      if (accessList.getListe() != null && accessList.getListe().size() == 2) {
        CertificatAcces certicate = (CertificatAcces) accessList.getListe().get(1);
        request.getSession().setAttribute("lastLogin", certicate);
      }
    }

  }

}
